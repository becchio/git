Serveur
===

Dépôt nu
---

```bash
git clone --bare projet projet.git
# == à
cp -R projet/.git projet.git
```

Demmarrage ssh
---

```bash
docker exec -it git_server service ssh start
docker exec -it git_server service ssh status
```

Copie sur le serveur
---

```bash
scp -r projet.git greg@172.17.0.2:/srv/git
# ou bien
docker cp projet.git git_server:/srv/git

```

Docker
===

build
---

```bash
docker build .
```

run
---
```bash
docker run --name git_server -it 38e2c1b97948 /bin/bash
```

exec
---
```bash
docker start git_server
docker exec -it git_server /bin/bash
```
