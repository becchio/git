#!/bin/bash
## Fonctions communes a tous les programmes de release.sh

export WARN_COLOR="[\033[33mNT\033[00m]";
export INFO_COLOR="[\033[32mINFO\033[00m]";
export ERROR_COLOR="[\033[31mERROR\033[00m]";


adddate() {
	echo "$(date)" "$1" >>  ${2};
}

log_error() {
	if [ "$1" = "" ]; then
		ERROR_PATH="/var/log/bi/ERROR.log";
	else
		ERROR_PATH=${1};
	fi
	if [ "$2" = "" ]; then
		MESSAGE="Unknow ERROR";
	else
		MESSAGE=${2};
	fi
	if [ "$3" = "" ]; then
		LEVEL="1";
	else
		LEVEL=${3};
	fi

	echo -e "${ERROR_COLOR} - exit ${LEVEL} - ${MESSAGE}.";

	adddate "[ERROR_PATH]- exit ${LEVEL} - >>${MESSAGE}<<" $ERROR_PATH;
}