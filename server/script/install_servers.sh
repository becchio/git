#!/bin/bash
## Installe tout l'envirennement pour tester git server

. $(dirname $PWD/${0})/tools.sh;

ERROR_PATH="/var/log/rssi/ERROR.log";

set +e;

docker stop git_server && docker rm git_server;

docker build .;
erreur="$?";
echo ${erreur};
if [ "$erreur" -gt 0 ]; then
	log_error ${ERROR_PATH} "Impossible de builder l'image" ${erreur};
	exit ${erreur};
fi

image_id=`docker build .|awk 'END {print}'|cut -d" " -f3`;
erreur="$?";
echo ${erreur};
if [ "$erreur" -gt 0 ]; then
	log_error ${ERROR_PATH} "Impossible de récupérer l'ID de l'image" ${erreur};
	exit ${erreur};
fi

docker run --name git_server -d ${image_id};
erreur="$?";
echo ${erreur};
if [ "$erreur" -gt 0 ]; then
	log_error ${ERROR_PATH} "Impossible de runner git_server" ${erreur};
	exit ${erreur};
fi

docker start git_server;
erreur="$?";
echo ${erreur};
if [ "$erreur" -gt 0 ]; then
	log_error ${ERROR_PATH} "Impossible de de lancer git_server" ${erreur};
	exit ${erreur};
fi
